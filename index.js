const Koa = require('koa')
const app = new Koa()
const { execSync } = require('child_process')
const url = require('url')
const got = require('got')

const botUrl =
  'https://open.feishu.cn/open-apis/bot/v2/hook/cdf29402-a8af-441c-96c7-9cc5c7aa1359'

const resolvePost = req =>
  new Promise(resolve => {
    let chunk = ''
    req.on('data', data => {
      chunk += data
    })
    req.on('end', () => {
      resolve(JSON.parse(chunk))
    })
  })
app.use(async ctx => {
  console.log('receive request')
  console.log(`url`, ctx.url)
  const params = url.parse(ctx.url, true).query
  const port = params?.port
  if (ctx.method === 'POST' && ctx.url === `/?port=${port}`) {
    const data = await resolvePost(ctx.req)
    console.log(`data`, data)
    // // 删除镜像
    // execSync(
    //   `docker image rm $(docker image ls -q ${data.repository?.repo_name})`,
    //   {
    //     stdio: 'inherit',
    //   }
    // )
    // 拉取镜像
    execSync(
      `docker pull ${data.repository?.repo_name}:${data?.push_data?.tag}`,
      {
        stdio: 'inherit',
      }
    )

    // 销毁 docker 容器
    execSync(
      `docker ps -a -f "name=^${data.repository.name}-container" --format="{{.Names}}" | xargs -r docker stop | xargs -r docker rm`,
      {
        stdio: 'inherit',
      }
    )

    // 创建 docker 容器
    execSync(
      `docker run -d -p ${port}:80 --name ${data.repository.name}-container ${data.repository.repo_name}:${data?.push_data?.tag}`,
      {
        stdio: 'inherit',
      }
    )
    await got.post(botUrl, {
      json: {
        msg_type: 'text',
        content: {
          text: `${data.repository.name}:${data?.push_data?.tag}发布成功`,
        },
      },
      responseType: 'json',
    })
    console.log('deploy success')
  }
  ctx.body = 'ok'
})

app.listen(3000, () => {
  console.log('listening server http://localhost:3000')
})
